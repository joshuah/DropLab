/* eslint-env node, browser, mocha */
/* global loadPage, expect, sinon */
require('./feature_helper');

describe('DropLab', function () {
  loadPage('/button_dropdown_list.html');

  beforeEach(function(){
    var stub = function () {};
    var DropLab = this.DropLab = require('../src/droplab')({
      CustomEvent: this.window.Event,
      HookButton: stub,
      HookInput: stub,
      window: this.window,
    });

    this.droplab = new DropLab();
  });

  describe('#plugin', function () {
    it('queues the plugin', function () {
      var plugin = function () {};
      this.droplab.plugin(plugin);
      expect(this.droplab.plugins).to.include(plugin);
    });
  });

  describe('#init', function () {
    it('initializes plugins', function () {
      var spy = sinon.spy();
      this.droplab.plugins = [spy];
      this.droplab.init();
      expect(spy).to.be.calledWith(this.DropLab);
    });

    it('dispatches a ready.dl event', function (done) {
      this.window.addEventListener('ready.dl', function() {
        done();
      });
      this.droplab.init();
    });

    it('sets ready to true', function () {
      this.droplab.init();
      expect(this.droplab.ready).to.be.true;
    });

    it('adds any queued data', function () {
      sinon.stub(this.droplab, 'addData');
      this.droplab.queuedData = [['main', [{
        id: 24603,
        text: 'Johan',
      }, {
        id: 24604,
        text: 'Justin',
      }]]];

      this.droplab.init();

      expect(this.droplab.addData).to.be.calledWith('main', [{
        id: 24603,
        text: 'Johan',
      }, {
        id: 24604,
        text: 'Justin',
      }]);

      this.droplab.addData.restore();
    });

    it('clears queued data', function () {
      this.droplab.queuedData = [['main', [{
        id: 24603,
        text: 'Johan',
      }, {
        id: 24604,
        text: 'Justin',
      }]]];

      this.droplab.init();

      expect(this.droplab.queuedData).to.eql([])
    });
  });

  describe('#addData', function () {
    context('when DropLab is not in the ready state', function () {
      it('queues the data to be added later', function () {
        this.droplab.addData('main', [{
          id: 24603,
          text: 'Johan',
        }, {
          id: 24604,
          text: 'Justin',
        }]);

        expect(this.droplab.queuedData).to.eql([['main', [{
          id: 24603,
          text: 'Johan',
        }, {
          id: 24604,
          text: 'Justin',
        }]]]);
      });
    });

    context('when DropLab is in the ready state', function () {
      beforeEach(function(){
        sinon.stub(this.droplab, '_addData');
        this.droplab.init();
      });

      afterEach(function(){
        this.droplab._addData.restore();
      });

      it('immediately adds the data', function () {
        this.droplab.addData('main', [{
          id: 24603,
          text: 'Johan',
        }, {
          id: 24604,
          text: 'Justin',
        }]);

        expect(this.droplab._addData).to.be.calledWith('main', [{
          id: 24603,
          text: 'Johan',
        }, {
          id: 24604,
          text: 'Justin',
        }]);
      });
    });
  });
});
