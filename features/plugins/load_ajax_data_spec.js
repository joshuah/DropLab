/* eslint-env node, mocha */
/* global loadPage, expect */
require('../feature_helper');

var fs = require('fs');
var path = require('path');

var pluginSrc = fs.readFileSync(path.resolve(__dirname, '../../dist/plugins/ajax.datasource.js')).toString();
var exampleData = fs.readFileSync(path.resolve(__dirname, './example_data.json')).toString();

describe('dynamically setting data', function() {

  loadPage('/features/plugins/load_ajax_data.html', pluginSrc);

  beforeEach(function(){
    var xhr;
    this.winEval(function() {
      xhr = this.sinon.useFakeXMLHttpRequest();
    });

    var requests = this.requests = [];
    xhr.onCreate = function(request) {
      requests.push(request);
    };
  });

  context('if the ajax request succeeds', function() {
    beforeEach(function(done){
      this.winEval(function (w) {
        w.droplab.addData('main', '/features/plugins/example_data.json');
      });
      this.requests[0].respond(200, {}, exampleData);

      setTimeout(done, 200);
    });

    it('sets the list items in the page', function () {
      var content = this.$('ul[data-dynamic]').innerHTML;
      expect(content).to.equal('<li><a href="#" data-id="24601">Jerry</a></li><li><a href="#" data-id="24602">Jimmy</a></li>');
    });
  });

  context('if the ajax request fails', function() {
    beforeEach(function(){
      this.winEval(function (w) {
        w.droplab.addData('main', '/features/plugins/bad_file.json');
      });
      this.requests[0].respond(404, {}, "Not Found");
    });

    it('does not set the list items in the page', function () {
      var content = this.$('ul[data-dynamic]').innerHTML;
      expect(content).to.not.equal('<li><a href="#" data-id="24601">Jerry</a></li><li><a href="#" data-id="24602">Jimmy</a></li>');
    });
  });

});
