/* global droplab */
droplab.plugin(function init(DropLab) {

  var keydown = function keydown(e) {
    var list = e.detail.hook.list;
    var data = list.data;
    var value = e.detail.hook.trigger.value.toLowerCase();
    var config;
    var matches = [];
    // check if no matches
    var regexp = new RegExp(value, 'gi');
    var check_matches = false;
    // iterate over each data object and check is it contain input value

    // will only work on dynamically set data
    if(!data){
      return;
    }
    config = droplab.config[e.detail.hook.id];
    matches = data.map(function(o){
      if(o.text.match(regexp)){
        check_matches = true;
      }
      // hide object from list if it doesn't match input value AND if object is not error message
      if(o.id !== 0) {
        o.droplab_hidden = o[config.text].toLowerCase().indexOf(value) === -1;
      }
      return o;
    });
    // if input value was not found in data -> show error message
    if(!check_matches){
      matches[0].droplab_hidden = false;
    }
    // id input value is empty -> hide error message
    if(value.trim().length === 0){
      matches[0].droplab_hidden = true;
    }
    list.render(matches);
  }

  window.addEventListener('keyup.dl', keydown);
});
